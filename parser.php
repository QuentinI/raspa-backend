<?php

//XuQn8nhce4YW

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Excel2007;
use PhpOffice\PhpSpreadsheet\Cell;
use Medoo\Medoo;

$IN_DEV = isset($_ENV["DEV"]);

$database = NULL;

if ($IN_DEV) {
  $database = new Medoo([
    'database_type' => 'sqlite',
  	'database_file' => 'raspa.db'
  ]);
} else {

}

// Download spreadsheet
$fileName = __DIR__ . '/schedule.ods';
$url = "https://docs.google.com/spreadsheets/d/139zViuFU2BMZpbgGGBnGq5I_V0oz6zwA-1blxQc1qG4/export?format=ods";

$file = file_get_contents($url);
file_put_contents($fileName, $file);

// Open spreadsheet
$spreadsheet = PhpOffice\PhpSpreadsheet\IOFactory::load($fileName);

$raspa = [];

foreach ($spreadsheet->getAllSheets() as $worksheet) {

  $title = $worksheet->getTitle();
  $today = date("d.m.y");
  $groups = [];


  // "Unmerge" cells
  //     A   B            A   B
  //   + - + - +        + - + - +
  // 1 | X | Y |      1 | X | Y |
  //   + - + - +  =>    + - + - +
  // 2 |       |      2 | Z | Z |
  //   |   Z   |        + - + - +
  // 3 |       |      3 | Z | Z |
  //   + - - - +        + - + - +
  foreach ($worksheet->getMergeCells() as $merged) {
      list($start, $end) = explode(':', $merged);
      $start = PhpOffice\PhpSpreadsheet\Cell::coordinateFromString($start);
      $end = PhpOffice\PhpSpreadsheet\Cell::coordinateFromString($end);
      $startCol = PhpOffice\PhpSpreadsheet\Cell::columnIndexFromString($start[0]);
      $startRow = $start[1];
      $endCol = PhpOffice\PhpSpreadsheet\Cell::columnIndexFromString($end[0]);
      $endRow = $end[1];
      $val = $worksheet->getCellByColumnAndRow($startCol-1, $startRow)->getValue();
      for ($col = $startCol; $col < $endCol; ++$col) {
          for ($row = $startRow; $row <= $endRow; ++$row) {
              $worksheet->getCellByColumnAndRow($col, $row)->setValue($val);
          }
      }
  }

  $highestRow = $worksheet->getHighestRow();
  $highestColumn = $worksheet->getHighestColumn();
  $highestColumnIndex = Cell::columnIndexFromString($highestColumn);

  for ($row = 1; $row < $highestRow; ++$row) {
      // if next row starts with "1" - which is "first lesson"
      if ($worksheet->getCellByColumnAndRow(0, $row+1)->getValue() == 1) {
          $grouprow = new PhpOffice\PhpSpreadsheet\Worksheet\Row($worksheet, $row);
          foreach ($grouprow->getCellIterator() as $cell) {
              if (preg_match("/(\d|\d\d)(.*)-(1|2)/", $cell->getValue())) {
                 $group = [
                     "name" => $cell->getValue(),
                     "lessons" => []
                 ];
                 $j = $row + 1;
                 $cell_col = PhpOffice\PhpSpreadsheet\Cell::columnIndexFromString($cell->getColumn()) - 1;
                 while (is_numeric($worksheet->getCellByColumnAndRow(0, $j)->getValue())) {
                     $room = $worksheet->getCellByColumnAndRow($cell_col + 1, $j)->getValue();
                     if (! preg_match("(\d\d|кк|зал)", $room)) {
                         $room = "";
                     }
                     $lesson = [
                         "order" => $worksheet->getCellByColumnAndRow(0, $j)->getValue(),
                         "name" => $worksheet->getCellByColumnAndRow($cell_col, $j)->getValue(),
                         "room" => $room
                     ];
                     array_push($group["lessons"], $lesson);
                     $j += 1;
                 }
                 $i = count($group["lessons"]);
                 while ($i-->0 && is_null($group["lessons"][$i]["name"])) { // This made my day: http://stackoverflow.com/questions/12342095/php-foreach-loop-through-array-in-opposite-order#comment16570227_12342177
                     unset($group["lessons"][$i]);
                 }
                 array_push($groups, $group);
             }
          }
      }
  }
  if ($groups) {
    $raspa[$title] = $groups;
  }
}

file_put_contents(__DIR__."/".$today.".json", json_encode($raspa));

?>
